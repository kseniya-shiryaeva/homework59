import React, {Component} from 'react';

class FilmForm extends Component {
    render() {
        const {onSubmit} = this.props;
        return (
            <form id="film_form" className="FilmForm" onSubmit={onSubmit}>
                <input type="text" name="title" id="title"/>
                <button type="submit">Add</button>
            </form>
        );
    }
}

export default FilmForm;