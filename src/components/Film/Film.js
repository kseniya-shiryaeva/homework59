import React, {Component} from 'react';

class Film extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.title !== this.props.title;
    }

    render() {
        const {title, id, onChange, onClick} = this.props;
        return (
            <div className="Film">
                <input type="text" name="film_name" id={id} value={title} onChange={onChange}/><button type="button" id={id} onClick={onClick}>x</button>
            </div>
        );
    }
}

export default Film;