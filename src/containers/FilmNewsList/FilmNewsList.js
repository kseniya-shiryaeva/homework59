import React, {Component} from 'react';
import FilmForm from "../../components/FIlmForm/FilmForm";
import './FilmNewsList.css';
import Film from "../../components/Film/Film";
import {nanoid} from "nanoid";

class FilmNewsList extends Component {
    state = {
        films: [
            {title: 'Free Guy', id:nanoid()},
            {title: 'Cruella', id:nanoid()},
            {title: 'The Gentlemen', id:nanoid()},
        ]
    };

    render() {
        const onInputChange = e => {
            const newTitle = e.target.value;
            const id = e.target.id;
            const newState = this.state.films.map(item => {
                if (item.id === id) {
                    item.title = newTitle;
                }
                return item;
            });
            this.setState({films: newState});
        };

        const removeFilm = e => {
            const id = e.target.id;
            const newFilms = this.state.films.filter(item => item.id !== id);
            this.setState({
                ...this.state,
                films: newFilms
            });
        };

        const addFilm = e => {
            e.preventDefault();
            const newFilms = [
                ...this.state.films,
                {title: e.target.title.value, id: nanoid()}
            ];
            this.setState({
                ...this.state,
                films: newFilms
            });
        }

        return (
            <div className="FilmNewsList">
                <FilmForm onSubmit={e => addFilm(e)} />
                <div className="films">
                    {this.state.films.map(film => (
                        <Film title={film.title} key={film.id} id={film.id} onChange={e => onInputChange(e)} onClick={e => removeFilm(e)} />
                    ))}
                </div>
            </div>
        );
    }
}

export default FilmNewsList;