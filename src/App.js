import React, {Component} from 'react';
import FilmNewsList from "./containers/FilmNewsList/FilmNewsList";

class App extends Component {
    render() {
        return (
            <div className="App">
              <FilmNewsList />
            </div>
        );
    }
}

export default App;